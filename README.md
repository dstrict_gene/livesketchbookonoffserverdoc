# Live Sketchbook OnOff Restful Server Documentation
made in c++ using boost

## Usage
- Release/LiveSketchbookOnOff.exe MY_STATIC_IP_ADDR AbsolutePathToUnityEXE

argv[0]: LiveSketchbookOnOff.exe

argv[1]: 유니티가 돌아갈 컴퓨에 지정된 static ip

argv[2]: 유니티 실행파일의 전체 파일 주소


## Install

1. Release/run_lsb_server.bat를 수정 해주세요

```
:: run_lsb_server.bat
:: OnOffServer.bat
:: example
LiveSketchbookOnOff 10.10.90.31 C:\dstrict\unity\EmailSenderBuild\EmailSender.exe
```
2. 바로 가기를 만들어 시작 폴더에 넣어 주세요.
    1. run_lsb_server.bat의 바로가기를 마우스 우클릭을 통해 생성합니다.
    2. window key + R 을 눌러 실행창을 띄웁니다.
    3. shell:startup 을 치면 시작 폴더가 열립니다.
    4. run_lsb_server.lnk 바로가기 파일을 옮겨주세요.


## API
run_lsb_server.bat에서 지정된 ip에서 8086의 포트로 서버가 컴퓨터가 시작되면 자동실행됩니다. (PC_IP:8086) (주소는 설치 이후 static ip로 지정될 예정)

4가지의 REST API가 있으며, response는 JSON으로 보냅니다.

1. start: 나이트 사파리를 실행합니다. 실행 이후 1초 뒤 화면을 최상위로 올립니다.
    - success(NEWLY_STARTED): 나이트 사파리를 실행합니다.
    - fail(ALREADY_STARTED): 이미 실행 중이라면 무시합니다.
    - fail(FAILED_TO_OPEN): 파일 실행 실패

2. kill: 나이트 사파리를 종료합니다.
    - success(CLOSED_SUCCESSFULLY)나이트 사파리를 종료합니다.
    - fail(NOT_RUNNING) 종료할 나이트 사파리가 없으면 무시합니다.

3. status: 나이트 사파리가 실행중인지 체크 합니다.
    - success(RUNNING) 돌아가는 중
    - success(NOT_RUNNING) 안 돌아가는 중

4. top: 유니티를 윈도우 최상위로 올립니다.
    - success(BROUGHT_TO_TOP) 최상위로 올림
    - fail(NOT_RUNNING) 안 돌아 가는중

## Example
### 1. start: 나이트 사파리를 실행합니다.

예제 IP: 10.10.90.31

**GET**
```
10.10.90.31:8086/start
```
**Response**

정상적으로 새로 유니티를 틀었을 경우
```
{"status":"success","data":"NEWLY_STARTED"}
```
이미 유니티가 돌고 있을 경우, 아무일도 일어 나지 않습니다.
```
{"status":"fail","data":"ALREADY_STARTED"}
```
실행파일을 열지 못했을 경우
```
{"status":"fail","data":"FAILED_TO_OPEN"}
```
 
### 2. kill: 나이트 사파리를 종료합니다.
**GET**
```
10.10.90.31:8086/kill
```
**Response**

정상적으로 종료되었을 경우
```
{"status":"success","data":"CLOSED_SUCCESSFULLY"}
```
나이트 사파리가 돌고 있지 않아 아무것도 끄지 않았을 경우
```
{"status":"fail","data":"NOT_RUNNING"}
```

### 3. status: 나이트 사파리가 돌고 있는지 확인합니다.
**GET**
```
10.10.90.31:8086/status
```
**Response**

나이트 사파리가 돌고 있는 경우
```
{"status":"success","data":"RUNNING"}
```
나이트 사파리가 안 돌고 있는 경우
```
{"status":"success","data":"NOT_RUNNING"}
```

### 4. top: 나이트 사파리를 최상위로 올립니다.
***GET***
```
10.10.90.31:8086/top
```
**Response**

최상위로 올립니다.
```
{"status":"success","data":"BROUGHT_TO_TOP"}
```
나이트 사파리가 안 돌고 있는 경우
```
{"status":"fail","data":"NOT_RUNNING"}
```


### 5.다른 GET Request를 받을 경우
**Response**
```
{"status":"fail","data":"COMMAND_NOT_FOUND"}
```
